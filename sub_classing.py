from cls_instance import Employee

# Subclassing

class Developer(Employee):  # Developer is a subclass of Employee class which have all the info of parent class
    	pass

class Developer_2(Employee):

	payment = 500  # change superclass variable without effecting in superclass

	def __init__(self, first, last, salary, prog_lng):
		super().__init__(first, last, salary)
		self.prog_lng = prog_lng

class Manager(Employee):
	"""docstring for Manager"""

	payment = 3000

	def __init__(self, first, last, salary, employees=None):
		super().__init__(first, last, salary)
		if employees is None:
			self.employees = []
		else:
			self.employees = employees

	def add_emp(self, emp):
		if emp not in self.employees:
			self.employees.append(emp)

	def remove_emp(self, emp):
		if emp in self.employees:
			self.employees.remove(emp)

	def print_emps(self):
		for e in self.employees:
			print('--->' + e.full_name())
		


dev_emp_1 = Developer('Md', 'Shefar', 300000)
dev_emp_2 = Developer('Md', 'Small', 50000)

dev_2_emp_1 = Developer_2('Ms', 'Sddad', 4000, 'Python')
dev_2_emp_2 = Developer_2('Ns', 'hdgyu', 4000, 'Java')

print(Developer_2.payment)
print(dev_emp_1.full_name())
print(Developer.num_of_employee)

print(dev_2_emp_1.full_name() + ' and Email : ' + dev_2_emp_1.email)
print(dev_2_emp_1.prog_lng)

mng_1 = Manager('Md', 'Small', 50000, [dev_emp_1])

print(mng_1.full_name())
mng_1.print_emps()
mng_1.add_emp(dev_emp_2)
print('After Adding employee 2 ')
mng_1.print_emps()
print('After removing employee 1 ')
mng_1.remove_emp(dev_emp_1)
mng_1.print_emps()
print(mng_1.payment)