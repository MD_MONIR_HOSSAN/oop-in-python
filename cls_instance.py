"""
# Regular Method
	
	Take self as first argument which has the instance of that class

# Classs Method
	
	Take class as first argument and work as altranative constructor

# Static Method

	Take no argument except his own argument and work as normal methos

"""

class Employee:

	# Class Variable
	payment = 0
	num_of_employee = 0

	def __init__(self, first, last, salary):
		#instance Variable
		self.first = first
		self.last = last
		self.salary = salary
		self.email = first + '.'  + last + '@email.com'

		Employee.num_of_employee += 1

	#Regular method
	def full_name(self):     
		return self.first + ' ' + self.last

	#Regular method
	def total_pay(self):
		return self.salary + self.payment #to access class variable


	# Class Method
	@classmethod
	def raise_amount(cls, pay):
		cls.payment = pay

	@classmethod
	def emp_to_str(cls, emp_str):
		first, last, pay = emp_str.split('-')
		return cls(first, last, pay)

	@staticmethod
	def is_work_day(day):
		if day.weekday() == 5 or day.weekday() == 6:
			return False
		return True


##########################################################################################################

emp_1 = Employee('Md' , 'Raju', 30000)
emp_2 = Employee('Ms', 'Rah', 4000)

# print(help(Employee))
# print(Employee.__dict__)

print(Employee.num_of_employee)

print(emp_1.full_name())
print(emp_2.email)
print(Employee.full_name(emp_1))

Employee.payment = 100    #accessing class variable

print(emp_1.total_pay())
print(emp_2.total_pay())

cls_method = Employee.raise_amount(10)
print(emp_1.payment)
print(Employee.payment)

emp_str_1 = 'Md-Raju-12000'

from_str = emp_1.emp_to_str(emp_str_1)

print(emp_1.full_name())
print(emp_1.email)


import datetime
a_date = datetime.date(2017, 7, 3)

print(Employee.is_work_day(a_date))

#################################################################################################













